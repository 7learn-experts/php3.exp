<?php
include_once "../bootstrap/init.php";
sleep(1);
$action_whitelist = array('add', 'remove', 'update', 'report');
$action = $_POST['action'];
if (isPostRequest() && in_array($action, $action_whitelist)) {
    switch ($action) {
        case 'remove':
            echo $cdb->remove(['id' => $_POST['id']]);
            redirect('');
            break;
        case 'add':
            $newContact = array(
                'contact_name' => $_POST['contact_name'],
                'contact_number' => $_POST['contact_number'],
                'category' => $_POST['category']
            );
            if ($cdb->add($newContact)) {
                echo "Added Successfully";
            } else {
                echo "Add Error !";
            }
            break;
        default:
            echo "action not implemented yet";
    }
}
