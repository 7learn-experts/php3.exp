<?php

include_once "constants.php";
include_once BASE_PATH . "vendor/autoload.php";

if (IS_DEV_MODE) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}
include_once "helpers.php";
include_once BASE_PATH . "Utilities/View.php";
include_once BASE_PATH . "DB/Base_Model.php";
include_once BASE_PATH . "DB/Contact.php";

$cdb = new Contact(get_config('db'));
