<?php

class View
{
    public static function load($file_path)
    {
        // if (self::cache_exist($file_path)) {
        //     // load from cache folder
        // }

        $theme_file = BASE_PATH . "themes/" . ACTIVE_THEME . "/$file_path";
        include_once $theme_file;
    }


    public static function render($file_path)
    {
        ob_start();
        $theme_file = BASE_PATH . "themes/" . ACTIVE_THEME . "/$file_path";
        include_once $theme_file;
        $tpl_html = ob_get_clean();
        // self::save_cache($file_path, $tpl_html);
        return $tpl_html;
    }

    public static function save_cache($file_path, $tpl_html)
    {
        file_put_contents(BASE_PATH . "cache/" . md5($file_path) . ".cache", $tpl_html);
    }
}
