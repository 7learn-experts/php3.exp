<?php
include "vendor/autoload.php";

use Carbon\Carbon;

use Hekmatinasser\Verta\Verta;

$verta = new Verta();

echo $verta->addDays(10)->format('%d %B %Y') . "<br>";
echo Verta::persianNumbers(Verta::now());
echo Carbon::now()->addDay(20);
