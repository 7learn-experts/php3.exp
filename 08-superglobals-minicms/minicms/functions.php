<?php
function getPosts($return_assoc = 0)
{
    $posts  = json_decode(file_get_contents(POST_DB), $return_assoc);
    return $posts;
}

// $post->{title,author,content}
function savePost(stdClass $post): bool
{
    $posts  = getPosts(1);
    $posts[] = (array)$post;
    $posts_json = json_encode($posts);
    file_put_contents(POST_DB, $posts_json);
    return true;
}


function isAdmin($user_name = 0)
{
    return 1;
}

function siteUrl($uri = '')
{
    return BASE_URL . $uri;
}
