<?php
include_once "init.php";
$posts = getPosts();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mini Cms PhpExp3</title>
    <link href="https://s.7learn.com/themes/slt/assets/build/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
    <?php if (isAdmin()) : ?>.
        <div>
            <a href="<?= siteUrl('panel') ?>">پنل مدیریت</a>
        </div>
    <?php endif; ?>

    <?php foreach ($posts as $pid => $post) : ?>
        <div class='post-box'>
            <span class="author"><?= $post->author ?></span>
            <h2><?= $post->title ?></h2>
            <div class="content">
                <?= $post->content ?>
            </div>
        </div>
    <?php endforeach; ?>

</body>

</html>