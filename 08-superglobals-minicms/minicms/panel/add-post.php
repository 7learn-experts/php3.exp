<?php include_once "init.php"; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mini Cms Panel > Add Post</title>
    <link href="https://s.7learn.com/themes/slt/assets/build/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/styles.css">
    <style>
        body * {
            display: block;
            direction: rtl;
            margin: 10px auto;
        }
    </style>
</head>

<body>
    <form action="process/save-post.php" method="POST">
        <input type="text" name='title' placeholder="عنوان پست">
        <select name="author">
            <option value="لقمان">لقمان</option>
            <option value="علی">علی</option>
            <option value="نسرین">نسرین</option>
            <option value="زهرا">زهرا</option>
            <option value="تقی">تقی</option>
        </select>
        <textarea name="content" cols="30" rows="10"></textarea>
        <button type="submit">ذخیره</button>
    </form>
</body>

</html>