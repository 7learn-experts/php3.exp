<?php
include_once "config.php";
include_once "functions.php";
include_once "process.php";

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Task manager UI</title>



  <link rel="stylesheet" href="assets/style.css">


</head>

<body>


  <div class="page">
    <div class="pageHeader">
      <div class="title">
        <a href="http://php3.exp/7task/">Home</a>

      </div>
      <div class="userPanel"><i class="fa fa-chevron-down"></i><span class="username">John Doe </span><img src="assets/marde.jpg" width="40" height="40" /></div>
    </div>
    <div class="main">
      <div class="nav">
        <div class="searchbox">
          <div><i class="fa fa-search"></i>
            <input type="search" placeholder="Search" />
          </div>
        </div>
        <div class="menu">
          <div class="title">Navigation</div>
          <ul>
            <li> <i class="fa fa-home"></i>Home</li>
            <li><i class="fa fa-signal"></i>Activity</li>
            <li class="active"> <i class="fa fa-tasks"></i>Manage Tasks</li>
            <li> <i class="fa fa-envelope"></i>Messages</li>
          </ul>
        </div>
      </div>
      <div class="view">
        <div class="viewHeader">
          <div class="title">Manage Tasks</div>
          <div class="functions">
            <div class="button active" id="addTaskBtn">Add New Task</div>
            <div class="button"><a href="?status=1">Draft</a></div>
            <div class="button"><a href="?status=2">Todo</a></div>
            <div class="button"><a href="?status=3">Done</a></div>
            <div class="button"><a href="?status=4">Archive</a></div>
            <div class="button inverz"><i class="fa fa-trash-o"></i></div>
          </div>
        </div>
        <div class="content">
          <div class="list">
            <div class="title">Today</div>
            <ul>
              <?php foreach ($tasks as $task) : $task = (object) $task; ?>
                <li class="checked">
                  <?php
                  if ($task->status == 3) {
                    echo '<i class="fa fa-check-square"></i>';
                  } else {
                    echo '<i class="fa fa-square"></i>';
                  }
                  ?>
                  <span><?= $task->title ?></span>
                  <div class="info">
                    <a href="http://php3.exp/7task/?del=<?= $task->id ?>"><i style='color: #b61010;' class="fa fa-trash-o"></i></a>
                    <div class="button green"><?= $task_statuses[$task->status] ?></div>
                    <span>Created at <?= $task->created_at ?></span>
                  </div>
                </li>
              <?php endforeach; ?>

            </ul>
          </div>
          <div class="list">
            <div class="title">Tomorrow</div>
            <ul>
              <li><i class="fa fa-square-o"></i><span>Find front end developer</span>
                <div class="info"></div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal-wrapper">
    <div class="modal-inner">
      <span class="close">X</span>
      <form action="ajaxProcess.php" id='addForm'>
        <input type="text" name='title' placeholder="Title">
        <select name="owner">
          <option value="Owner">Owner</option>
          <option value="Ali">Ali</option>
          <option value="Taghi">Taghi</option>
          <option value="Maryam">Maryam</option>
          <option value="Sara">Sara</option>
        </select>
        <button type="submit">Add Task</button>
        <div class="result"></div>
      </form>
    </div>
  </div>
  <script src='assets/jquery.min.js'></script>


  <script>
    // $("CSS Selector").event(.modal - wrapper);
    $(".modal-wrapper .close").click(function() {
      $(".modal-wrapper").fadeOut();
    });

    $("#addTaskBtn").click(function() {
      $(".modal-wrapper").slideDown();
    });

    $("#addForm").submit(function(e) {
      e.preventDefault();
      var form = $(this);
      var resultBox = form.find('.result');
      resultBox.html("<img src='assets/loading.gif'>");
      $.ajax({
        type: 'POST',
        url: form.attr('action'),
        data: form.serialize(),
        timeout: 20000,
        success: function(response) {
          resultBox.html(response);
        },
        error: function() {
          resultBox.html("Error");
        }
      });

    });
  </script>

  <script src="assets/script.js"></script>




</body>

</html>