<?php

include 'config.php';
include 'functions.php';


is_ajax_request() or exit();


sleep(1);
if (strlen($_POST['title']) <= 1) {
 echo "Please Enter the Title !";
 die();
}

$owner_whitelist = ['Ali', 'Taghi', 'Sara', 'Maryam'];
if (in_array($_POST['owner'], $owner_whitelist)) {
 if (add_task($_POST['title'], $_POST['owner'])) {
  echo "Task added Successfully !";
 } else {
  echo "Some Errors ...";
 }
} else {
 echo "Error: Invalid Owner!";
}
