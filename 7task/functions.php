<?php


function is_ajax_request()
{
 return ('XMLHttpRequest' == ($_SERVER['HTTP_X_REQUESTED_WITH'] ?? ''));
}

function delete_task(int $task_id)
{
 global $conn;
 $sql = "delete from " . TASK_TABLE . " where id={$task_id}";

 $conn->query($sql);
 return $conn->affected_rows;
}

function add_task($title, $owner)
{
 global $conn;
 $sql = "INSERT INTO " . TASK_TABLE . " (`title`, `owner`) VALUES ('$title', '$owner');";

 $conn->query($sql);
 return $conn->affected_rows;
}

function get_tasks()
{
 global $conn;

 $where = '';
 if (isset($_GET['status'])) {
  $where = 'where status = ' . $_GET['status'];
 }
 $sql = "SELECT * FROM " . TASK_TABLE . " $where";


 $result = $conn->query($sql);

 return $result->fetch_all(MYSQLI_ASSOC);
}
