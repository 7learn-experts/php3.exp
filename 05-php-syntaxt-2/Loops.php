<?php
include "db.php";
/*
// Loops
// while
// do while
// for
// for(each)

echo "<hr>While<br>";
$i = 0;
while (1) {
    $i++;
    if ($i == 7) {
        break;
    }
    echo "<span style='color:red'> $i</span><br>";
}

echo "<hr>Do..While<br>";
$i = 0;
do {
    $i++;
    if ($i == 7) {
        break;
    }
    echo "<span style='color:red'> $i</span><br>";
} while (1);



echo "<hr>For<br>";
for ($i = 10; $i >= 0; $i--) {
    echo "<span style='color:red'> $i</span><br>";
}

echo "<hr>For Users<br>";
$users2 = [["ali", 25], ["maryam", 26], ["ali", 25], ["maryam", 26], ["hasan", 65]];

for ($j = 0; $j < sizeof($users2); $j++) {
    echo "{$users2[$j][0]} : {$users2[$j][1]}<br>";
}

*/

echo "<hr>Foreach<br>";
foreach ($users as $user_first_name => $userData) {
    echo " $user_first_name ({$userData['age']})<br>";
}
