<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);


$variableName = "variableValue";

$int_var = 26;           // like age
$age = $int_var;         // pass by reference
$age = &$int_var;        // pass by value
$float_var = 19.75;      // like mark
$string_var = 19.75;     // like UserName/UserEmail/Title
$null_var = null;        // like jib
$boolean_var = false;    // like is_male,is_night,is_king,is_logged_in
/* PHP False Values : 
    the boolean FALSE itself
    the integers 0 and -0 (zero)
    the floats 0.0 and -0.0 (zero)
    the empty string "" or '', and the string "0"
    an array with zero elements array() or []
    the special type NULL (including unset variables)
    SimpleXML objects created from empty tags
*/


// like students, numbers, users, months, years, days
$array_var1 = array(1, 2, 3, 4);
$array_var2 = [1, 2, 3, 4];
// indexes     0  1  2  3  ...
// $array_var2[2] = 777;
// echo $array_var2[2];

// this is associative array
$array_var3 = [1 => 24, "sara" => 18, 45, 46, "sara" => 52];
// var_dump($array_var3[2]);
// var_dump($array_var3["sara"]);
$users = array( //
    "ali" =>   ["name" => "ali", "age" => 25],
    "sara" =>  ["name" => "Sara", "age" => 19],
    "taghi" => ["name" => "Taghi", "age" => 98]
);

$user_id = "ali";
$user_name = $users[$user_id]["name"];
$user_age = $users[$user_id]["age"];

// echo "$user_name is $user_age years Old";
// var_dump($users);
/*
// array to json
$jsonStr = json_encode($users);
echo $jsonStr . "<br><br>";
// json to objects/arrays
$userObjects = json_decode($jsonStr);
echo ($userObjects->sara->age);
echo ($userObjects->ali->name);

// php Objects
$user1 = new stdClass;
$user1->name = "Loghman";
$user1->age = 30;
$user1->birthday = "1367/03/16";
var_dump($user1->name);

// objects && arrays is call by reference in php
$user2 = $user1;
$user3 = clone $user1;
$user2->name = "Amir Loghman 2";
$user3->name = "Amir Loghman 3";
echo $user1->name . "<br>";
echo $user2->name;
echo $user1->name . "<br>";
echo $user3->name;


// Casting / Type Converting
$is_old = (int)null;
echo gettype($is_old) . "($is_old)";


var_dump($users);

$userObjects = (object)$users;
var_dump($userObjects);

$userArr = (array)$userObjects;
var_dump($userArr);
var_dump(settype($int_var, 'object'));
$int_var->value = $int_var->scalar;
var_dump($int_var);
*/

// php Constants in 2 ways
const PI = 3.14;
const MAX_CAPACITY = 32;
define('PI_D', 3.14);
define('MAX_CAPACITY_D', 3.14);

// Magic Constants
echo __DIR__ . "<br>";
echo __FILE__ . "<br>";
echo __LINE__ . "<br>";
echo __FUNCTION__ . "<br>";

// chack types
$a = 12.1;
$b = "-020";
var_dump(is_int($b));
var_dump(is_float($b));
var_dump(is_numeric($b));

$natinalCode = "12 3-45 5.6788";
$natinalCode = str_replace(['-', '.', '*', ' '], '', $natinalCode);
if (ctype_digit($natinalCode)) {
    echo "nc is Valid !";
} else {
    echo "InValid nc !";
}
echo $natinalCode;
