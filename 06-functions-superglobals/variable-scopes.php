<?php
include "../common.php";
/*
$a = 1;

function myFunc()
{
    global $a;
    static $s = 1;
    $s++;
    $a++;
    p("a= $a");
    p("s= $s");
    p("--------------");
}

myFunc();
$a = 10;
myFunc();
myFunc();
p($a);
p($x);

*/

// real usage of static var in function
$arr = ["a" => "ali", "b" => "hasan", "C" => "maryam"];
p(end($arr));
// p(next($arr));
// p(prev($arr));

$filePath = "http://php3.exp/06-functions-superglobals/variable-scopes.zip";

$dArr = explode(".", $filePath);
var_dump(end($dArr));


// gitlab.com
