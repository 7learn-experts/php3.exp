<?php

namespace Tests\Integration;

use App\Order;
use App\Product;
use App\Repositories\OrderRepository;
use PDO;
use PHPUnit\Framework\TestCase;
use Tests\Fakes\FakeMailer;

class OrderRepositoryTest extends TestCase
{
    public function setUp()
    { }
    public function tearDown()
    { }
    public static function setUpBeforeClass()
    { }
    public static function tearDownAfterClass()
    { }
    /**
     * it can make a new order and save in database
     * @test
     *
     * @return void
     */
    public function it_can_create_an_order()
    {
        $db = new PDO('mysql:host=localhost;dbname=shop', 'root', '');
        $fakeMailer = new FakeMailer();
        $orderRepository = new OrderRepository($db, $fakeMailer);
        $order = new Order();
        $product1 = new Product("first product", 150000);
        $product2 = new Product("second product", 350000);
        $order->addProduct($product1);
        $order->addProduct($product2);
        $order->setUserID(3);
        $order->calculateTotalPrice();
        $creationResult = $orderRepository->create($order);
        $this->assertTrue($creationResult);
    }
    /**
     * @test
     *
     * @return void
     */
    public function it_must_thorw_an_exception_id_invalid_data_is_passed()
    {
        $this->expectException(\Exception::class);
        $db = new PDO('mysql:host=localhost;dbname=shop', 'root', '');
        $orderRepository = new OrderRepository($db);
        $order = new Order(null, null);
        $creationResult = $orderRepository->create($order);
        $this->assertTrue($creationResult);
    }
}
