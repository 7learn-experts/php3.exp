<?php

namespace Tests\Fakes;

use App\Mailer;

class FakeMailer extends Mailer
{
    public function send(string $email, string $message)
    {
        return true;
    }
}
