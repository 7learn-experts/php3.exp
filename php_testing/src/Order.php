<?php

namespace App;

class Order
{
    private $orderID;

    private $userID;

    private $totalPrice;

    private $products = [];

    public function addProduct(Product $product)
    {
        $this->products[] = $product;
    }

    public function getProducts()
    {
        return $this->products;
    }
    public function calculateTotalPrice()
    {
        $this->totalPrice = array_reduce($this->getProducts(), function ($total, $product) {
            return $total += $product->getPrice();
        }, 0);
    }

    public function getTotalPrice()
    {
        return $this->totalPrice;
    }
    public function getUserID()
    {
        return $this->userID;
    }
    public function setUserID(int $userID)
    {
        $this->userID = $userID;
    }
    public function getOrderID()
    {
        return $this->totalPrice;
    }
    public function setOrderID(int $orderID)
    {
        $this->orderID = $orderID;
    }
}
