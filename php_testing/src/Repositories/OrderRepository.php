<?php

namespace App\Repositories;

use App\Mailer;
use App\Order;
use App\Product;
use Exception;
use PDO;

class OrderRepository
{
    private $db;
    private $mailer;
    public function __construct(PDO $pdo, Mailer $mailer)
    {
        $this->db = $pdo;
        $this->mailer = $mailer;
    }
    public function create(Order $order)
    {

        $sql = "INSERT INTO orders (user_id,total_price) VALUES (:user_id,:total_price)";
        $stmt = $this->db->prepare($sql);
        $userID = $order->getUserID();
        $totalPrice = $order->getTotalPrice();

        $stmt->bindParam(':user_id', $userID);
        $stmt->bindParam(':total_price', $totalPrice);
        if ($stmt->execute()) {
            $order->setOrderID((int) $this->db->lastInsertId());
            $this->mailer->send("sample@gmail.com", "Your order has placed!");
            return true;
        }
        throw new Exception('1024:Order Creation Failed!');
    }
}
