<?php

namespace App\Repositories;

use App\Product;
use Exception;
use PDO;

class ProductRepository
{
    private $db;
    public function __construct(PDO $pdo)
    {
        $this->db = $pdo;
        // $this->db = new PDO('mysql:host=localhost;dbname=shop', 'root', '');
    }
    public function create(Product $product)
    {

        $sql = "INSERT INTO products (title,price) VALUES (:title,:price)";
        $stmt = $this->db->prepare($sql);
        $title = $product->getTitle();
        $price = $product->getPrice();
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':price', $price);
        if ($stmt->execute()) {
            $product->setProductID($this->db->lastInsertId());

            return true;
        }
        throw new Exception('1023:Product Creation Failed!');
    }
}
